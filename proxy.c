/*
 * proxy.c - M1522.000800 System Programming: Web proxy
 *
 * Student ID: 2012-11853
 * Name: Seokjin Han
 *
 * The code implements a concurrent proxy server. When the client connects to
 * the proxy server, the main thread fetch the client's info and create new
 * thread which will process requests of the client. When the client writes
 * through the socket, the thread parses the request first. If the request is
 * not valid (cannot be parsed, for instance), return a help message to the
 * client. Otherwise, trying to connect the given server and deliver the
 * client's message. Then, the thread fetches a response from the server and
 * return to the client. The messages and corresponding infos will be logged on
 * the proxy server. This routine will continue until the socket disconnected
 * somehow.
 *
 */

#include "csapp.h"

/* The name of the proxy's log file */
#define PROXY_LOG "proxy.log"

/* Undefine this if you don't want debugging output */
#define DEBUG

/*
 * Functions to define
 */
void *process_request(void* vargp);
int open_clientfd_ts(char *hostname, int port, sem_t *mutexp);
ssize_t Rio_readn_w(int fd, void *ptr, size_t nbytes);
ssize_t Rio_readlineb_w(rio_t *rp, void *usrbuf, size_t maxlen);
void Rio_writen_w(int fd, void *usrbuf, size_t n);

/*
 * Arguments passed to threads
 */
struct client_info {
  char addr[16];
  int port;
  int connfd;
};

/*
 * POSIX semaphores
 */
sem_t mutex_client;
sem_t mutex_log;

/*
 * main - Main routine for the proxy program
 */
int main(int argc, char **argv)
{
  /* Check arguments */
  if (argc != 2) {
    fprintf(stderr, "Usage: %s <port number>\n", argv[0]);
    exit(0);
  }
  /* Initialize */
  int port = atoi(argv[1]);
  pthread_t tid;
  sem_init(&mutex_client, 0, 1);
  sem_init(&mutex_log, 0, 1);
  Signal(SIGPIPE, SIG_IGN);

  /* Handling connections from clients */
  int listenfd = Open_listenfd(port);
  while (1) {
    struct sockaddr_in clientaddr;
    struct client_info *client = Malloc(sizeof(struct client_info));
    int clientlen = sizeof(struct sockaddr_in);
    client->connfd = Accept(listenfd, (SA *) &clientaddr, &clientlen);
    client->port = ntohs(clientaddr.sin_port);
    strncpy(client->addr, inet_ntoa(clientaddr.sin_addr), 16);
    Pthread_create(&tid, NULL, process_request, client);
  }
}

/*
 * Handling requests from each connections
 */
void *process_request(void *vargp)
{
  FILE *log;
  time_t rawtime;
  struct tm timeinfo;
  char buf[MAXBUF], *msg, *tmp;

  // Fetch client infos
  char client_addr[16];
  rio_t client_rio, server_rio;
  int serverfd, clientfd, client_port;
  struct client_info *client = (struct client_info *)vargp;
  clientfd = client->connfd;
  client_port = client->port;
  strncpy(client_addr, client->addr, 16);
  Pthread_detach(pthread_self());
  Free(vargp);

  Rio_readinitb(&client_rio, clientfd);
  while (Rio_readlineb_w(&client_rio, buf, MAXBUF) != 0) {
    // Parse client message & obtain server infos
    msg = strpbrk(buf, " ");
    if (msg == NULL) {
      Rio_writen_w(clientfd, "proxy usage: <host> <port> <message>\n",
                   strlen("proxy usage: <host> <port> <message>\n"));
      continue;
    }
    *msg = 0;
    do {
      ++msg;
    } while (*msg == ' ');
    tmp = msg;
    msg = strpbrk(msg, " ");
    if (msg == NULL) {
      Rio_writen_w(clientfd, "proxy usage: <host> <port> <message>\n",
                   strlen("proxy usage: <host> <port> <message>\n"));
      continue;
    }
    *msg = 0;
    do {
      ++msg;
    } while (*msg == ' ');

    // Connect to server, deliver client's message and fetch response
    serverfd = open_clientfd_ts(buf, atoi(tmp), &mutex_client);
    if (serverfd < 0) {
      Rio_writen_w(clientfd, "cannot connect to server\n",
                   strlen("cannot connect to server\n"));
      continue;
    }
    Rio_readinitb(&server_rio, serverfd);
    Rio_writen_w(serverfd, msg, strlen(msg));
    if (Rio_readlineb_w(&server_rio, buf, MAXBUF) == 0) {
      Close(serverfd);
      continue;
    }
    Close(serverfd);
    Rio_writen_w(clientfd, buf, strlen(buf));

    // Get current time as string
    tmp = malloc(80);
    if (time(&rawtime) != (time_t)-1 &&
        localtime_r(&rawtime, &timeinfo) != NULL) {
      strftime(tmp, 80, "%a %d %b %Y %T %Z", &timeinfo);
    } else {
      tmp = NULL;
    }

    // Logging to file; lock-and-copy the unsafe i/o event
    P(&mutex_log);
    log = fopen(PROXY_LOG, "a");
    fprintf(log, "%s: %s %d %d %s\n",
            tmp, client_addr, client_port, strlen(buf), buf);
    fclose(log);
    V(&mutex_log);
    free(tmp);
  }
  Close(clientfd);
  return NULL;
}

/*
 * thread-safe wrapper of open_clientfd
 */
int open_clientfd_ts(char *hostname, int port, sem_t *mutexp)
{
  int clientfd;
  struct hostent *hp;
  struct sockaddr_in serveraddr;

  if ((clientfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
    return -1;
  }

  bzero((char *) &serveraddr, sizeof(serveraddr));
  serveraddr.sin_family = AF_INET;
  serveraddr.sin_port = htons(port);

  // lock-and-copy gethostbyname since it's unsafe
  P(mutexp);
  hp = gethostbyname(hostname);
  if (hp == NULL) {
    V(mutexp);
    return -2;
  }
  bcopy((char *)hp->h_addr_list[0],
        (char *)&serveraddr.sin_addr.s_addr, hp->h_length);
  V(mutexp);

  if (connect(clientfd, (SA *) &serveraddr, sizeof(serveraddr)) < 0) {
    return -1;
  }
  return clientfd;
}

/*
 * non-terminating wrapper of rio_readn
 */
ssize_t Rio_readn_w(int fd, void *ptr, size_t nbytes)
{
  ssize_t n;
  if ((n = rio_readn(fd, ptr, nbytes)) < 0) {
    fprintf(stderr, "Rio_readn_w error: %s\n", strerror(errno));
    return 0;
  }
  return n;
}

/*
 * non-terminating wrapper of rio_readlineb
 */
ssize_t Rio_readlineb_w(rio_t *rp, void *usrbuf, size_t maxlen)
{
  ssize_t rc;
  if ((rc = rio_readlineb(rp, usrbuf, maxlen)) < 0) {
    fprintf(stderr, "Rio_readlinb_w error: %s\n", strerror(errno));
    return 0;
  }
  return rc;
}

/*
 * non-terminating wrapper of rio_writen
 */
void Rio_writen_w(int fd, void *usrbuf, size_t n)
{
  if (rio_writen(fd, usrbuf, n) != n)
    fprintf(stderr, "Rio_writen_w error: %s\n", strerror(errno));
}
